from st3m.application import Application, ApplicationContext
from st3m.ui.view import Context, InputState
from st3m.input import InputController
import leds
import math

LED_COLORS = [
    [1, 0, 0], # Red
    [1, 0.5, 0], # Yellow
    [0, 0, 1], # Blue
    [0, 0.8, 0], # Green
]
HAT_COLORS = [
    [1, 0.8, 0], # Yellow
    [0.9, 0, 0], # Red
    [0.2, 0.7, 0], # Green
    [0, 0.26, 0.663], # Blue
]
PROPELLER_COLOR = [0, 0, 0.663] # Blue

LEDS_PER_GROUP = 8
LED_MAX_GROUPS = 5
LED_ROTATION_SPEED = 400
PROPELLER_DEFAULT_SPEED = 0.35
PROPELLER_TARGET_SPEED = 0.25
PROPELLER_DECAY_RATE = 0.005

class ChaosmentorsApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.input = InputController()
        self._led_groups = [
            [i for i in range(j * LEDS_PER_GROUP, (j + 1) * LEDS_PER_GROUP)] for j in range(LED_MAX_GROUPS)
        ]
        self._color_index = 0
        self._last_delta_ms = 0
        self._time_passed = 0
        self._propeller_angle = 0
        self._propeller_speed = PROPELLER_TARGET_SPEED
        self._target_speed = PROPELLER_TARGET_SPEED
        self._propeller_fast_spin_time = 0

        leds.set_slew_rate(180)

    def on_enter(self, vm) -> None:
        self.update_leds()
    
    def draw_background_pattern(self, ctx: Context, propeller_radius=140):
        for i in range(6):
            ctx.begin_path()
            ctx.move_to(0, 0)
            ctx.line_to(propeller_radius * math.cos(i * math.pi / 3), propeller_radius * math.sin(i * math.pi / 3))
            ctx.line_to(propeller_radius * math.cos((i + 1) * math.pi / 3), propeller_radius * math.sin((i + 1) * math.pi / 3))
            ctx.close_path()

            # Fill with propeller colors
            color = HAT_COLORS[i % len(HAT_COLORS)]
            ctx.rgb(*color).fill()


    def update_leds(self):
        for idx, leds_leaf in enumerate(self._led_groups):
            color = LED_COLORS[(self._color_index + idx) % len(LED_COLORS)]
            for led in leds_leaf:
                leds.set_rgb(led, *color)
        leds.update()
    
    def draw_propeller(self, ctx, propeller_radius, propeller_thickness, num_blades=2):
        ctx.rotate(self._propeller_angle)
        
        # Draw propeller blades
        for _ in range(num_blades):
            ctx.begin_path()
            ctx.move_to(0, -propeller_thickness / 2)
            ctx.line_to(propeller_radius - 10, -propeller_thickness / 4)
            ctx.arc_to(propeller_radius, 0, propeller_radius - 10, propeller_thickness / 4, propeller_radius - 30)
            ctx.line_to(0, propeller_thickness / 2)
            ctx.close_path()

            # Fill with blue color
            ctx.rgb(*PROPELLER_COLOR)
            ctx.fill()

            # Rotate for the next blade
            ctx.rotate(2 * math.pi / num_blades)

    def draw(self, ctx: Context) -> None:
        # Paint the background
        self.draw_background_pattern(ctx)

        # Draw propeller
        propeller_radius = 50
        propeller_thickness = 4
        self.draw_propeller(ctx, propeller_radius, propeller_thickness)
        
        # Add red dot
        ctx.rgb(1, 0, 0).round_rectangle(-10, -10, 20, 20, 20).fill()

        self.update_leds()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)  # Let BaseView do its thing

        self._time_passed += delta_ms

         # Smoothly transition current speed toward the target speed
        self._propeller_speed += (self._target_speed - self._propeller_speed) * PROPELLER_DECAY_RATE
        
        # Update angle based on the combined speed (default + dynamic)
        angle_increment = (PROPELLER_DEFAULT_SPEED + self._propeller_speed) * delta_ms / 1000
        self._propeller_angle += angle_increment

        if self._last_delta_ms >= LED_ROTATION_SPEED:
            self._color_index = (self._color_index + 1) % len(LED_COLORS)
            self._last_delta_ms = 0

        self._last_delta_ms = self._last_delta_ms + delta_ms

        for i in range(10):
            petal = self.input.captouch.petals[i].whole
            if petal.pressed:
                self._target_speed += 0.9
                self._propeller_fast_spin_time += 0.1
        
        direction = ins.buttons.app
        if direction == ins.buttons.PRESSED_LEFT:
            self._target_speed += 0.3
            self._propeller_fast_spin_time += 0.1
        elif direction == ins.buttons.PRESSED_RIGHT:
            self._target_speed -= 0.3
            self._propeller_fast_spin_time += 0.1
        
        if self._propeller_fast_spin_time > 0:
            self._propeller_fast_spin_time -= 0.005

            if self._propeller_fast_spin_time > 1:
                self._propeller_fast_spin_time = 1
        else:
            self._propeller_fast_spin_time = 0
            self._target_speed = PROPELLER_DEFAULT_SPEED
        
# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(ChaosmentorsApp(ApplicationContext()))
